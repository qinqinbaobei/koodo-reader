export interface ViewAreaProps {
  currentEpub: any;
  handleFetchLocations: (currentEpub: any) => void;
}
export interface ViewAreaStates {
  isShowImage: boolean;
  imageRatio: string;
}
